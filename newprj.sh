#!/bin/bash

numargs=$#
theargs=$*

if [ $numargs -ne 1 ]; then
  echo "Usage: newprj.sh ProjName"
  exit
fi

TODAY=`date +%Y-%m-%d`
TODAY_MD=`date +%B\ %d,\ %Y`
YEAR=`date +%Y`

PROJECT=$1

## where you want new project dirs to be created under
DEV_HOME=~/Work/Proyectos

## for the markdown bits
AUTHOR="Facundo Muñoz"

##

if [ -d $DEV_HOME/$PROJECT ]
then
	echo "Warning: project directory already exists."
	read -p "Continue and overwrite? [y/n] " -n 1 -r
	echo
	if [[ ! $REPLY =~ ^[Yy]$ ]]
	then
		exit
	fi
fi

## -r: recursive: copy directories recursively
## -L: dereference: always follow symbolic links
## -n: no-clobber: do not overwrite existing files
## cp -rLn ~/Work/templates/project $DEV_HOME/$PROJECT
rsync -rpgo --exclude='.git' ~/Work/templates/project/ $DEV_HOME/$PROJECT

cd $DEV_HOME/$PROJECT


## Substitute placeholders project_name, author_name and current_date
## with current values
## Use double quotes to make the shell expand variables while preserving whitespace
## https://askubuntu.com/questions/76808/how-do-i-use-variables-in-a-sed-command
sed -i "s/project_name/$PROJECT/g" README.md _targets.R src/project_name.Rmd
sed -i "s/author_name/$AUTHOR/g" README.md src/project_name.Rmd
sed -i "s/current_date/$TODAY_MD/g" README.md

## Rename files
mv project_name.Rproj $PROJECT.Rproj
mv src/project_name.Rmd src/$PROJECT.Rmd
mv src/project_name.bib src/$PROJECT.bib

## If git --version > 2.28
## https://superuser.com/questions/1419613/change-git-init-default-branch-name#1572156
## git init --initial-branch="main"
git init
git symbolic-ref HEAD refs/heads/main
git add .
git commit -m "Setup project."

xdg-open $PROJECT.Rproj

exit 0
