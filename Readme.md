# newprj

A bash script to initialize new projects

## Usage:

`> newproj.sh ProjectName`

This will:

- Create a new directory ProjectName in your projects' directory (currently hardcoded to `~/Work/Proyectos`)

- Create subdirectories `src`, `data`, `doc` and `reports` and populate them with files from a project-template in `~/Work/templates/project`.

- Write a template for `README.Rmd`

- Set up a RStudio project

- Initialize a `git` repository with some common ignored extensions and a first commit

- Open the project in RStudio


## TODO

* Have the default locations of the project directory and the project template be configurable 
    * environment variable? configuration dot-file in the home-dir?

* Allow specifying destination directory in the command-line, overriding default setting
